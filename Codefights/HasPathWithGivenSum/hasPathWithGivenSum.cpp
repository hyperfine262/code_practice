//
// Definition for binary tree:
// template<typename T>
// struct Tree {
//   Tree(const T &v) : value(v), left(nullptr), right(nullptr) {}
//   T value;
//   Tree *left;
//   Tree *right;
// };

bool hasPathWithGivenSum(Tree<int> * t, int s) {
    if (!t){
        if (s == 0) return true;
        return false;
    }
    std::queue< Tree<int> * > q;
    q.push(t);
    while ( !q.empty() ){
        auto current = q.front();
        if (!current->left && !current->right){
            if (current->value == s) return true;
        }
        if (current->left){
            current->left->value += current->value;
            q.push(current->left);
        }
        if (current->right){
            current->right->value += current->value;
            q.push(current->right);
        }
        q.pop();
    }
    return false;
}
