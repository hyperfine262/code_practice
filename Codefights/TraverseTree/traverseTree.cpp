//
// Definition for binary tree:
// template<typename T>
// struct Tree {
//   Tree(const T &v) : value(v), left(nullptr), right(nullptr) {}
//   T value;
//   Tree *left;
//   Tree *right;
// };

std::vector<int> traverseTree(Tree<int> * t) {
    std::vector<int> result;
    if (!t) return result;
    std::queue< Tree<int> * > q;
    q.push(t);
    while (!q.empty() ){
        auto current = q.front();
        q.pop();
        result.push_back( current->value );
        if ( current->left ){
            q.push( current->left);
        }
        if ( current->right ){
            q.push( current->right );
        }
    }
    return result;
}
