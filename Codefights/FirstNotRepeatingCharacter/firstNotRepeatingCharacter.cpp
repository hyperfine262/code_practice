char firstNotRepeatingCharacter(std::string s) {
    vector<int> times_seen(26,0);
    vector<char> discovery_order;
    unordered_set<char> candidates;
    for (auto c:s){
        int index = int(c) - 97;
        times_seen[index] ++;
        if (times_seen[index] == 1){
            discovery_order.push_back(c);
        }
    }
    for(int i = 0; i < 26; ++i){
        if ( times_seen[i] == 1 ){
            candidates.insert( char(i + 97) );
        }
    }
    for(char c:discovery_order){
        //if c is in candidates, return c;
        if (candidates.find(c) != candidates.end() ){
            return c;
        }
    }
    return '_';
}
