bool containsDuplicates(std::vector<int> a) {
    std::unordered_set<int> seen;
    for (auto num:a){
        if ( seen.insert(num).second == false ) return true;
    }
    return false;
}
