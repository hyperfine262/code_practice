func firstDuplicate(a: [Int]) -> Int {
    for i in 0 ... a.count-1 {
        var index = abs(a[i])-1
        if a[index] < 0 {return abs(a[index])}
        a[index] = -a[index]
    }
    return -1
}
