std::vector<std::vector<int>> rotateImage(std::vector<std::vector<int>> a) {
    int n = int(a.size());
    vector<vector<int>> result;
    for(int i=0; i < n; ++i){
        vector<int> row(n,0);
        result.push_back(row);
    }
    for( int i = 0; i < n; ++i){
        for ( int j = 0; j < n; ++j){
            int k = n - (i + 1);
            result[j][k] = a[i][j];
        }
    }
    return result;
}
